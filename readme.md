# Django-Tutorial

  

#### Table des matières
- [Introduction](#introduction)
- [Installation](#installation)
- [Configuration](#configuration)
- [Swagger](#swagger)
- [classes](#classes)
- [Postman](#postman)
- [postgresql](#postgres)
- [JWT](#jwt)
- [démonstration](#demonstration) 
- [git instructions](#git)
- [concepts](#concepts)
 


<a name="#introduction"></a>
### 1. Introduction
Django-tutorial est un tutoriel qui est destiné aux étudiants, afin de leurs permettre de débuter sur Django.
A la fin de ce tutoriel les étudiants auront crée un petit site de e-commerce qui permet de gérer des livres.

<a name="#installation"></a>
### 2. Installation

- Tous d'abord il faut installer python (https://www.python.org/downloads)
- Mettre python en tant que variable d'environnement.
- Si vous ne l'avez pas deja, il faudra installer [pip](https://pypi.org/project/pip/), qui servira lors des installations.
- Mettre pip en tant que variable d'environnement.

Pour la suite nous auront besoin de [VirtaulEnv](https://virtualenv.pypa.io/en/latest/) pour pouvoir travailler sans déreglé les parametres de votre PC.
 Utilisez `python get-pip.py` pour l'installer. <br>
 Un répertoir nommer env s'est alors crée dans répertoire personnel.
 Pour activer l'environnement virtuel il faut lancer `source .venv/bin/activate` dans votre répertoire de travail.<br>
 (env) doit maintenant figurer avant votre chemin dans le terminal.

Une fois l'environnement virtuel installer on peut commencer à installer nos paquêts python, ainsi que notre projet django.

Une fois Python, pip et virtualEnv sont bien installés, nous allons installer [Django](https://www.djangoproject.com/).\
Pour cela : 
* ouvrez votre Terminal avec votre environnement virtuel et exécutez la commande suivante `pip install django==2.1.5`.
* Pour vérifier que l'installation est réussie, exécutez `pip list` vous devrez voir Django dans cette liste avec la version 2.1.5. ou tapez `python -m django --version`
* Notez qu'au moment de la réalisation de ce tutoriel la dernière version de django est 2.1.5 mais elle peut évoluer, pour cela je vous invite à toujours vérifier la [dernière version](https://www.djangoproject.com/download/)

Pour ce tutoriel nous avons utilisé Visual studio code que vous pouvez télécharger [ici](https://code.visualstudio.com/download)

<a name="#configuration"></a>
### 3. Configuration

Depuis la ligne de commande, déplacez-vous dans le répertoire ou vous voulez stocker votre code puis exécutez la commande suivante :
`
        django-admin startproject mysite 
`

Cette commande crée un nouveau projet utilisant le nom donné en paramètre, ici mysite. 
Ensuite exécutez la commande suivante :
`
python manage.py startapp myapp
`

cette commande crée une nouvelle application dans votre projet. 

Maintenant dans Visual studio : 

- accédez à mysite (le nom de notre projet) et accédez à settings dans la partie installed_apps.\
rajoutez 

```
    'myapp.apps.MyappConfig',
```

avec myapp le nom de notre application, apps et une classe de cette application et MyappConfig une méthode de cette classe\
- finalement dans mysite-> urls.py rajoutez :
```
urlpatterns = [
    path('myapp/', include("myapp.urls")),
```

<a name="#swagger"></a>
### 4. Swagger

Swagger UI est un logiciel basé sur les technologies du web (HTML, Javascript, CSS) permettant de générer une documentation en utilisant les spécifications d’OpenAPI.
Pour commencer nous devons installer [Django Rest Swagger](https://django-rest-swagger.readthedocs.io/en/latest/)
dans votre environnement virtuel et exécutez la commande suivante : `pip install django-rest-swagger`\
ensuite dans INSTALLED_APPS ajoutez : `'rest_framework_swagger'`
Enfin dans la classe mysite/urls.py ajoutez ceci :
```
from rest_framework_swagger.views import get_swagger_view

schema_view = get_schema_view(title='Test', renderer_classes=[OpenAPIRenderer, SwaggerUIRenderer])

urlpatterns = [
      path('api/',schema_view,name="docs"),
    ]
```
Pour vérifier si tout fonctionne correctement, exécutez la commande `python manage.py runserver`
et dans votre fenêtre tapez `http://127.0.0.1:8000/api/`, si la page se lance correctement c'est que vous l'avez bien configurer.

<a name="#postgres"></a>
### 5. Postgresql

Vous devez tout d'abord [télécharger postgresql](https://www.postgresql.org/download/)
Déplacez vous vers le répertoire `PostgreSQL\postgresqlVersion\bin`
dans le terminal veuillez exécutez `psql` pour pouvoir envoyer des commandes à postgreSQL\
ensuite créez votre base de données et un utilisateur avec les deux commandes suivantes 
```
# CREATE USER name;
# CREATE DATABASE databaseName OWNER name;

```
Après la creation de votre base de données, 
dans le fichier  mysite/settings.py trouvez 
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
```
et remplacez le par 
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'databaseName',
        'USER': 'name',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '',
    }
}
```
finalement afin d'utiliser la nouvelle base de données, nous avons besoin de réaliser les migrations en exécutant la commande suivante : 
`
python manage.py migrate
`
<a name="#jwt"></a>
### 6.JWT

JWT est un jeton permettant d’échanger des informations de manière sécurisée.

Pour commencer exécutez la commande suivante : `pip install djangorestframework_simplejwt==3.3` 

Ensuite dans mysite/settings.py rajoutez ce bout de code 
```
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ],
}
```
et dans mysite/urls.py rajoutez ce bout de code 
```
from rest_framework_simplejwt import views as jwt_views

urlpatterns = [
   
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]
```
exécutez ensuite les deux commandes 
- `python manage.py makemigrations` 
- `python manage.py migrate`

Rajoutez dans myapp/views.py la commande ` permission_classes = (IsAuthenticated,)` avant le début de chaque méthode.

Retournez sur  `http://127.0.0.1:8000/api/`
vous trouverez normalement deux rubriques (api et swagger).

Cliquez sur api, et choisissez `/api/token/ ` vous serez ensuite invité à entrer un nom d'utilisateur et password.
Si vous entrez les bon identifiants, dans le body vous devez trouvez un access suivi d'une chaine de caractère (notre token), copier le puis en haut à droite de votre fenetre
vous trouverez un bouton Authorize, cliquez dessus et dans le champs value
écrivez Bearer **suivie du token que vous venez de copier coller** puis appuyez sur authorize, si tout est bien configuré vous devriez voir la rubrique book apparaitre.

<a name="#classes"></a>
### 5. Classes
Dans cette sections nous allons configurer les différentes classes de myapp.
<a name="#urls"></a>
##### 1. urls.py : 
dans la classe urls.py rajoutez ce bout de code.
```
from django.urls import path
from .views import *
from rest_framework_swagger.views import get_swagger_view


urlpatterns = [
    # serve index.htmlss
    path('',index, name='index'),

    #[GET POST books] + [GET, PUT, DELETE]
    path('books/', BookList.as_view(), name="book-list"),
    path('books/<int:pk>', BookDetail.as_view(), name="book-details"),
]
```
<a name="#views"></a>
##### 2. views.py
dans la classe views.py  rajoutez ce bout de code 
```
from django.http import HttpResponse, JsonResponse, Http404

from rest_framework.views import APIView
from rest_framework import status

from .models import Book
from .serializers import BookSerializer

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

#create your views here

def index(request) :
    return HttpResponse ("Hello, world. You're la moula")

class BookList(APIView):
    """
    List all books, or create a new book
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        books = Book.objects.all()
        serializer = BookSerializer(books, many=True)
        return JsonResponse(serializer.data, safe=False)

    def post(self, request, format=None):
        #.data est le body de la requête
        serializer = BookSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, safe=False, status=status.HTTP_201_CREATED)
            #safe=FALSE, multiplicité de livre ? 
        return JsonResponse(serializer.errors, safe=False, status=status.HTTP_400_BAD_REQUEST)
    

class BookDetail(APIView):
    """
    Retrieve,  update or delete a book instance,
    """
    permission_classes = (IsAuthenticated,)

    def get_object(self,pk):
        try:
            return Book.objects.get(pk=pk)
        except Book.DoesNotExist:
            raise Http404

    def get(self,request,pk, format=None):
        book = self.get_object(pk)
        serializer = BookSerializer(book)
        return JsonResponse(serializer.data, safe=False)

    def put(self, request, pk, format=None):
        book = self.get_object
        serializer=BookSerializer(book, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, safe=False)
        return JsonResponse(serializer.errors, safe=False,status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        book = self.get_object(pk)
        book.delete()
        return JsonResponse({}, safe=False, status=status.HTTP_204_NO_CONTENT)
```
cette classe nous permet dedéfinir les trois méthode cité ci-dessous dans urls.py :
1. index
2. BookDetail
3. BookList

<a name="#ser"></a>
##### 3. serializers.py
 dans cette classe rentrez le code suivant 
 ```
from rest_framework import serializers
from .models import Book

class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = '__all__'
        #fields ('title', 'author', ...etc)
 ```
 cette classe nous permet de vérifier que les données envoyées à la base de données sont bien structurées.
<a name="#models"></a>
##### 4. models.py
 dans cette classe rentrez le code suivant 
 ```
 from django.db import models

# Create your models here.

class Book(models.Model):
    title = models.CharField(max_length=256, null=True, blank=True)
    author = models.CharField(max_length=256, null=True, blank=True)
    year = models.PositiveIntegerField(default=0, null=True, blank=True)
    price = models.FloatField(default=-1.0, null=True, blank=True)
    description = models.TextField(max_length=256, null=True, blank=True)
    bestseller = models.BooleanField(default=False)

 ```
 Cette classe créer le model des données à respecter pour notre base de données.
<a name="#admin"></a> 
##### 5. admin.py
Finalement dans admin ajoutez : 
```
from django.contrib import admin
from .models import Book
# Register your models here.

admin.site.register(Book)

```

<a name="#postman"></a>
### 6. PostMan
<br>
Postman permet de construire et d’exécuter des requêtes HTTP, de les stocker dans un historique afin de pouvoir les rejouer, mais surtout de les organiser en Collections. Cette classification permet notamment de regrouper des requêtes de façon « fonctionnelle » (par exemple enchaînement d’ajout d’item au panier, ou bien un processus d’identification).
Veuillez avant tout[télecharger](https://www.getpostman.com/downloads/) postman. 

Lancez Postman, et entrez l'adresse 
```
http://127.0.0.1:8000/myapp/books/
```
Vous pouvez choisir ensuite la méthode  souhaitée et exécuter votre requête.

<a name="#demo"></a>
### 7.Démonstration

Pour pouvoir observer ce que nous venons juste de faire, il faut
1. Exécutez la commande : `python manage.py migrate`
ce qui permet de Synchroniser l'état de la base de données avec l'ensemble actuel de modèles et de migrations.
2. Ensuite il faut créer un superuser avec la commande : `python manage.py createsuperuser` puis renseigner un nom, un mail et un mot de passe.
Un message "Superuser created successfully" doit apparaitre à la fin. 
3. Finalement démarrez le serveur avec la commande `python manage.py runserver`
nous aurons alors un message avec diverses informations 

>System check identified no issues (0 silenced).\
January 27, 2019 - 18:24:07\
**Django version 2.1.5**, using settings **'mysite.settings'**\
Starting development server at http://127.0.0.1:8000/
Quit the server with CTRL-BREAK.
en vous rendant sur [cette URL](http://127.0.0.1:8000) vous tombez tout d'abord sur une page 404 not found, qui vous demande de choisir admin ou myapp. 
1. [admin](http://127.0.0.1:8000/admin/) l'utilisateur est invité à entrer son nom et son mot de passe pour accéder après au site d'administration ou il peut gérer les utilisateurs et les groupes 
2. [myapp](http://127.0.0.1:8000/myapp/) On retombe sur notre page views qui contient l'index, la réponse de notre requête.
3. [api](http://127.0.0.1:8000/api/) On peut accéder à l'interface de swagger, pour l'authorisation ou pour envoyer des reqûetes
4. [myapp/books/](http://127.0.0.1:8000/myapp/books/) on accède ici à la liste des books.

<a name="#git"></a>
### 8. Instructions git 

Il faut se placer dans le dossier que l'on veut lier au répertoire distant.
```
cd chemin/vers/repertoire/contenant/monProjet
git init
git remote add origin lien/de/votre/repository/gitlab
```

Après avoir créer et relier notre repository, nous allons donc commiter les fihcies exitants dans le repository,\
pour cela nous allons réaliser une série de commandes :
```
      git status 
      git add . 
      git commit -m "ajoutez votre message ici"
      git push -u origin master
```
nous avons donc enregistré, et envoyé nos fichiers dans le repository distant à partir de notre repository local. 

<a name="#concepts"></a>
### 9. Concepts

#### - django-admin :
L’utilitaire de ligne de commande de Django pour les tâches administratives.
#### - manage.py :
Créé automatiquement dans chaque Django projet, 
 fait la même chose que django-admin mais s'occupe de quelques choses pour vous:
* Il met le paquet de votre projet sur sys.path.
* Il définit la variable d’environnement DJANGO_SETTINGS_MODULE afin qu’il pointe vers le fichier settings.py de votre projet.

### - Architecture interne :
* **Le répertoire interne mysite /**\
Est le package Python actuel de votre projet. Son nom est le nom du package Python que vous devrez utiliser pour importer quoi que ce soit à l’intérieur de celui-ci (par exemple, mysite.urls
* **mysite/__init__.py:**\
Un fichier vide qui indique à Python que ce répertoire doit être considéré comme un package Python.
* **mysite/settings.py:**\
configuration pour ce projet Django, pour plus d'information consulter [les paramètres Django](https://docs.djangoproject.com/en/2.1/topics/settings/)
* **mysite/urls.py:**\
Les déclarations d'URL pour ce projet Django; une "table des matières" de notre site propulsé par Django, allez sur [la partie URLs ](https://docs.djangoproject.com/en/2.1/topics/http/urls/) pour plus d'informations
