from django.urls import path
from .views import *
from rest_framework_swagger.views import get_swagger_view


urlpatterns = [
    # serve index.htmlss
    path('',index, name='index'),

    #[GET POST books] + [GET, PUT, DELETE]
    path('books/', BookList.as_view(), name="book-list"),
    path('books/<int:pk>', BookDetail.as_view(), name="book-details"),
]